# Changelog

## 0.4.2 (20240613) - Fix acknowledgment bug

- Fix acknowledgements for non-leaf nodes

The non-Xbar client is nonfunctional.

## 0.4.1 (20240612) - Bug fixes and minor improvements

- Xbar client show acknowledgements
- Improve error handling
- Disable non-functional delete command in CLI

## 0.4 (20231231) - Grouping and otherwise improved functionality

- Support group-by-attribute in both CLI and xbar clients
- Refactor status parsing
- Use icons for xbar instead of text
- Bug fixes and improvements

## 0.3 (20230612) - Optional functionality

- Pressing Option key while viewing node menu shows when each status was
  reported.
- Acknowledgements are also now available this way.
- Updated for recent API changes in Smash Server 0.9.

## 0.2 (20230220) - Client functionality

- CLI client for acknowledgements
- add acknowledgements to xbar client

## 0.1 (20221213) - Basic functionality

- smash-xbar client
- basic packaging and configuration
