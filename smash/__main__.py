# pylint:
#
"""
Smash main CLI process
"""
from smash import cli

# by default assume CLI client
if __name__ == '__main__':
    cli.main()
