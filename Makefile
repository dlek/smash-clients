CHANGELOG	:= CHANGELOG.md
SOURCES		:= smash/__init__.py smash/__main__.py smash/cli.py smash/config.py smash/smash.py smash/status.py smash/xbar.py
LATEST		:= $(shell ls -t $(SOURCES) | head -n1)
DATESTAMP	:= $(shell date -r $(LATEST) +%s | ./base36)
VERSION		:= $(shell git describe --tags | sed -e 's/^v//' -e 's/-/+/' -e 's/-/./g' -e 's/\.g.*$$/.$(DATESTAMP)/')
VERSION_FILE	:= smash/version.py
VERSION_IN_FILE := $(shell sed -n 's/^version = "\(.*\)"/\1/p' $(VERSION_FILE))
PACKAGE		:= smash_clients
PACKAGE_WHL	:= dist/$(subst -,_,$(PACKAGE))-$(VERSION)-py3-none-any.whl
PACKAGE_TAR	:= dist/$(PACKAGE)-$(VERSION).tar.gz
PACKAGES	:= $(PACKAGE_WHL) $(PACKAGE_TAR)

all: test $(PACKAGES)

showinfo:
	@echo Datestamp: $(DATESTAMP)
	@echo Version: $(VERSION)

notest: $(PACKAGES)

.PHONY: test
test:
	tests/test-all

$(VERSION_FILE): $(SOURCES)
	@echo Version: $(VERSION)
	@echo Version in file: $(VERSION_IN_FILE)
ifeq ($(VERSION),$(VERSION_IN_FILE))
	@echo No need to update $(VERSION_FILE)
else
	sed -i_backup 's/^version = .*/version = "$(VERSION)"/' $(VERSION_FILE) && rm smash/version.py_backup
endif

# ensure version file exists, but don't force rebuild if it's refreshed
$(PACKAGES): $(SOURCES) | $(VERSION_FILE)
	@echo Version: $(VERSION)
	python3 -m build

packages: $(PACKAGES)

install: $(PACKAGE_TAR)
	# want this to not be the venv version
	#python3 -m pip --disable-pip-version-check install dist/smash-clients-$(VERSION).tar.gz
	/usr/local/bin/pip3 --disable-pip-version-check install --break-system-packages $(PACKAGE_TAR)

# fails if version doesn't match release pattern, so as not to publish
# development version.  PyPi will reject local dev versions but better to
# catch it here.
.PHONY: checkversion
checkversion:
	@echo "Checking that $(VERSION) is a release version"
	@[[ $(VERSION) =~ ^([0-9]+\.)*[0-9]+$$ ]] || ( echo "It's not" ; false )
	@echo "Checking that $(VERSION) is in $(VERSION_FILE)"
	@grep -qw "$(VERSION)" "$(VERSION_FILE)"

publish-test: $(PACKAGES) checkversion
	python3 -m twine upload --repository smash-clients-test $(PACKAGES)

publish: $(PACKAGES) checkversion
	python3 -m twine upload --repository smash-clients $(PACKAGES)
